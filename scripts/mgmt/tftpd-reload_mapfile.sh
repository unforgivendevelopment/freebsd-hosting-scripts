#!/usr/bin/env bash
#
# scripts/mgmt/tftpd-reload_mapfile.sh
#
# (c) 2016 Gerad Munsch <gmunsch@unforgivendevelopment.com>
# This is a new project, and as such, will mature over time. By no means are
# any aspects or components of the project thoroughly tested, and accordingly,
# shall not be used in a production environment without extreme testing and
# scrutiny by the system administrator.
#
# No warranty is provided, and 100% of the responsibility for the use of this
# project lies upon the system administrator who installs this project.
#
# You've been warned.
#

if [ -f /var/run/tftpd.pid ]; then
	tftpd_pid="$(cat /var/run/tftpd.pid)"

	echo -e "\n\033[1m** Sending \033[36;1mSIGHUP\033[0m \033[1mto tftpd, with PID: \033[36;1m${tftpd_pid}\033[0m"

	kill -HUP $tftpd_pid
	sleep 1

	echo -e "\033[1m** Operation complete\033[0m\n"
	exit 0
else
	echo -e "\n\033[1m** \033[31;1mERROR:\033[0m \033[1mThe PID file \033[36;1m/var/run/tftpd.pid\033[0m \033[1mcould not be found. Is tftpd running?\033[0m\n"
	exit 1
fi
