# freebsd-hosting-scripts.sh
#
# (c) 2016 Gerad Munsch <gmunsch@unforgivendevelopment.com>
# This is a new project, and as such, will mature over time. By no means are
# any aspects or components of the project thoroughly tested, and accordingly,
# shall not be used in a production environment without extreme testing and
# scrutiny by the system administrator.
#
# No warranty is provided, and 100% of the responsibility for the use of this
# project lies upon the system administrator who installs this project.
#
# You've been warned.
#

if [ -d "${__INSTALL_BIN_PATH__}" ]; then
	PATH="${__INSTALL_BIN_PATH__}:${PATH}"
fi
